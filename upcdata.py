"""Get product data for items with specified UPC."""
from lxml import etree
from apiaccess import ApiAccess
import re
import time
import sys


class UpcData():

    """Handle UPCs and return descriptive data."""

    def __init__(self):
        """Start."""
        self.results = {
                   "No UPC": 0,
                   "No ASIN": 0,
                   "No MARC": 0,
                   "No Price": 0,
                   "Low Price": 0,
                   "Valuable Items": 0,
        }
        self.valuable_items = []
        self.values_histogram = {}
        self.errors = 0

    def get_prices(self, filepath, sleep_time=0, threshold_price=100,
                   display_all=False):
        """Get prices for each item.

        args:
            filepath(str): location of XML file to parse.
        kwargs:
            sleep_time(float): time to sleep after each API call.
            threshold_price(int): price above which to classify as "valuable"
            display_all(bool): show data or not about every result.
        """
        self.threshold_price = threshold_price
        self.filepath = filepath
        self._load_xml()
        self.display_all = display_all
        self.sleep_time = sleep_time
        self.count = 0
        for upc in self._extract_upc():
            self.count += 1
            if self.errors > 20:
                print "Processing Complete."
                sys.exit()
            if self.count % 100 == 0:
                print "*****{0}*****".format(self.count)
            if upc is not None:
                # print upc.text
                self._get_amazon_data(upc.text)
            else:
                self.results["No UPC"] += 1

            if self.price:
                self._process_price()

            else:
                self.results["No Price"] += 1

    def _load_xml(self):
        """Parse XML."""
        self.tree = etree.parse(self.filepath)

    def _extract_upc(self):
        """Yield UPCs one at a time."""
        for album in self.tree.iter("Album"):
            self.album = album
            yield album.find("Pr_Code")

    def _get_amazon_data(self, upc):
        """Use UPC to access data from amazon.

        args:
            upc(str): string to transform to ASIN
        """
        aa = ApiAccess()
        asin = aa.get_asin(upc)
        self.marc_data = None
        if asin:
            try:
                self.marc_data = aa.get_amazon_data(asin, sleep_time=self.sleep_time)\
                                   .get_text()

            except Exception as e:
                self.errors += 1
                time.sleep(60)
                "Error: {0}".format(e)
                self.marc_data = aa.get_amazon_data(asin, sleep_time=self.sleep_time)\
                                   .get_text()

            if self.marc_data:
                self._extract_price()
            else:
                "Processing failed for {0}".format(asin)
                self.results["No MARC"] += 1

        else:
            self.results["No ASIN"] += 1

    def _extract_price(self):
        """Extract price information from MARC string."""
        self.price = None
        for line in self.marc_data.split("\n"):
            if "|c" in line and line.startswith("020"):
                price_pattern = r'[0-9]*\.[0-9]{2}'
                price_match = re.search(price_pattern, line.split("|c")[1])
                if price_match:
                    self.price = float(price_match.group(0))
                break

    def _process_price(self):
        """Check price value."""
        self._store_value()
        if self.price >= self.threshold_price:
            self.results["Valuable Items"] += 1
            self._generate_item(valuable=True)

        else:
            self.results["Low Price"] += 1
            if self.display_all:
                self._generate_item()

    def _generate_item(self, valuable=False):
        """Get item details for 'valuable' item."""
        item = {}
        item["Price"] = self.price
        self._get_fields_from_marc(["title", "amazonlink"])
        item["MarcTitle"] = self.marctitle
        item["AmazonLink"] = self.amazon_link
        item.update(self._get_xml_data())
        if valuable:
            self.valuable_items.append(item)
        print self.count
        print u"${1}: {2} by {0}".format(item["Artist"], item["Price"], item["MarcTitle"])
        print item["AmazonLink"]

    def _get_fields_from_marc(self, fields):
        """Get title from marc string.

        args:
            fields(list): list of fields to return from MARC. Must be
                "title" or "amazonlink"
        """
        self.marctitle = "None found"
        self.amazon_link = "None provided"
        for line in self.marc_data.split("\n"):
            if "title" in fields:
                if line.startswith("245"):
                    start = 7  # the index of where title ($a) should begin.
                    if "|" in line:
                        end = line.index("|")
                        self.marctitle = line[start:end].strip()
                    else:
                        self.marctitle = line[start:].strip()

            if "amazonlink" in fields:
                if line.startswith("856"):
                    self.amazon_link = line.split("|u")[1].strip()

    def _get_xml_data(self):
        """Pull all data from XML album info."""
        item_data = {}
        for field in self.album.iter():
            item_data[field.tag] = field.text
        return item_data

    def _store_value(self):
        """Keep tally of item values."""
        rounded_price = int(round(self.price))
        self.values_histogram[rounded_price] = self.values_histogram.get(rounded_price, 0) + 1
