# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

class BS():
    """Use BeautifulSoup module to pull data from HTML."""

    def __init__(self, html):
        """
        Establish BeautifulSoup object.

        args:
        html(str) -- html to process
        """
        self.bs = BeautifulSoup(html, "lxml")

    def get_text_by_class(self, class_value):
        """
        Return text stored in the first instance of an object containing the specified class name.

        args:
        class_value(str) -- name of class attribute to search for.
        """
        return self.bs.find(class_=class_value).string

    def get_text_by_tag(self, tag):
        """
        Return all text from a given tag from the BS object. **Needs to be tested when there are multiple instances of a give tag.

        args:
        tag(str) -- name of a tag that occurs within the current bs object.
        """
        return getattr(self.bs, tag).string.encode("latin1").decode("utf-8")
