"""API access."""

import os
from webaccess import WebAccess
from bs4 import BeautifulSoup


class ApiAccess():
    """General class for accessing content via HTTP APIs."""
    def __init__(self):
        pass

    def get_asin(self, upc, sleep_time=0):
        """
        Access supplied URL and return html page content.

        args:
            upc(str) -- identifier to be converted.
        kwargs:
            sleep_time(float) -- time to sleep, in seconds, following HTTP request
        """
        base_url = "http://upctoasin.com"
        request_url = os.path.join(base_url, upc)
        asin = WebAccess.get_url(request_url, sleep_time)
        return asin

    def get_amazon_data(self, asin, sleep_time=0):
        """Take specified amazon ID and get MARC data.

        args:
            asin(str): unique identifier used by amazon.
        kwargs:
            sleep_time(float): time to wait making web request.
        """
        base_url = "http://chopac.org/cgi-bin/tools/az2marc.pl"
        args = {"kw": asin,
                "ct": "com",
                "lc": "0"}
        marc_page = WebAccess.get_url(base_url, sleep_time, data=args)
        if marc_page is not None:
            return self._extract_marc(marc_page)
        else:
            return None

    def _extract_marc(self, marc_page):
        """Parse MARC record out of HTML.

        args:
            marc_page(str): html content containing MARC within the 'mr' id.
        """
        soup = BeautifulSoup(marc_page, "lxml")
        return soup.find(id="mr")
