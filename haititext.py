"""Web access class specifically for the Marronnage transcript project."""
from webaccess import WebAccess
from bs import BS
import locale
import os
import codecs
import datetime
from lxml import etree
from formatobject.formatobject import FormatObject


class HaitiText(WebAccess):

    """Access data from site http://www.marronnage.info/"""

    def __init__(self):

        # [id] in url should be replaced with actual id.
        self.source_url = "http://www.marronnage.info/fr/lire.php?type=annonce&id=[id]"

    def get_all_announcements(self, range_begin=1, range_end=11000, write_tsv=True):
        """
        Retrieve all announcement ('annonce') pages from the source site for a given range.

        kwargs:
        range_begin(int) -- first ID from which to grab html
        range_end(int) -- last ID from which to grab html
        """
        self.announcement_data = []
        errors = 0
        for aid in range(range_begin, range_end + 1):
            self.id_data = {}
            self.id_data["id"] = str(aid)

            announcement_html = self.get_announcement(str(aid))
            self.id_data["url"] = self.announcement_url

            try:
                self.parse_html(announcement_html)
                self.announcement_data.append(self.id_data)
            except:
                errors += 1
                print "***Data collection failed for ID {0}***".format(aid)
            
            if aid % 100 == 0:
                print "Processed {0} Entries".format(aid)
        
        print "Process completed with {0} errors".format(errors)
        if write_tsv:
            fo = FormatObject()
            fo.make_results_tsv(self.announcement_data)


    def get_announcement(self, announcement_id):
        """
        Get single announcement by ID from source_page.

        args:
        announcement_id(str) -- announcement ID to replace the [id] segment of the source_url.
        """
        self.announcement_url = self.source_url.replace("[id]", announcement_id)
        return self.get_html(self.announcement_url)

    def parse_html(self, html):
        """
        Extract information from supplied html.

        args:
        html(str) -- Should contain full html for page to be parsed.
        """
        bs_html = BS(html)
        self.id_data["text"] = self.clean_text(bs_html.get_text_by_tag("p"))

        announcement_metadata = bs_html.get_text_by_class("ind")
        self.id_data["date"] = self.extract_date(announcement_metadata)
        self.id_data["formatted_date"] = self.standardize_date()


    def clean_text(self, text):
        """
        Remove whitespace.

        args:
        text(unicode) -- any string object.
        """
        return text.rstrip().lstrip().replace("\t", " ").replace("\n", " ")

    def extract_date(self, announcement_string):
        """
        Extract date from raw metadata string from HTML page.

        args:
        announcement_string(unicode) -- expected to follow standard structure from source site, i.e. "date | section | page | announcement#"
        """
        self.date = announcement_string.split("|")[0].rstrip().lstrip()
        return self.date

    def standardize_date(self):
        """Process class' date object -- which should be in French and look like '22 janvier 1766' -- into standard format."""

        locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
        date_format = "%d %B %Y"
        try:
            date_object = datetime.datetime.strptime(self.date.encode("utf-8"), date_format)
            return date_object.isoformat().split("T")[0]

        except:
            return self.date


class HaitiData(object):

    """Parse CSV file to add columns with add'l data."""

    def __init__(self, tsv_file):
        """Open CSV for processing.

        args:
            tsv_file(str): path to csv file.
        """
        self.tsv_file = tsv_file

    def add_ner_output(self, source_dir):
        """Add NER results.

        source_dir(str): path to dir containing NER output.
        """
        self.lines = []
        self.new_lines = 0
        self.file_not_found = 0
        self.source_dir = source_dir
        with codecs.open(self.tsv_file, "r", "UTF-8") as tsv:
            for line in tsv:
                self.ner_dict = {
                                 "LOCATION": [],
                                 "PERSON": [],
                                 "ORGANIZATION": [],
                                 "DATE": [],
                                 "TIME": [],
                                }
                self._find_ner_file(line)

        self._write_tsv()
        print "{0} records updated".format(self.new_lines)
        print "{0} records not found".format(self.file_not_found)

    def _write_tsv(self):
        """Write all lines to new TSV file."""
        ner_filepath = os.path.splitext(self.tsv_file)[0] + "_with_ner.tsv"
        with codecs.open(ner_filepath, "w", "utf-8") as ner_out:
            for line in self.lines:
                ner_out.write(line)

    def _find_ner_file(self, line):
        """Find file with NER data to match row.

        args:
            line(str): 1 line / record from the tsv.
        """
        datex = 3
        idx = 4
        data = line.split("\t")
        datev = data[datex].strip()
        idv = data[idx].strip()
        file_name = datev + "-" + idv + ".txt.kaf.xml"
        self.ner_file_path = os.path.join(self.source_dir, file_name)
        if os.path.exists(self.ner_file_path):
            self._process_ner_xml()
            self._create_new_line(line)
        else:
            # print "Couldn't find {0}".format(self.ner_file_path)
            self.lines.append(line)
            self.file_not_found += 1

    def _process_ner_xml(self):
        """Process NER xml."""
        with codecs.open(self.ner_file_path, "r", "utf-8") as ner_xml:
            tree = etree.parse(ner_xml)
            root = tree.getroot()
            for en in root.iter("entity"):
                entity_type = en.get("type")
                for ref in en.iterchildren():
                    ref_children = ref.getchildren()
                    for rc in ref_children:
                        if rc.tag == etree.Comment:
                            self.ner_dict[entity_type].append(rc.text)
                            break

    def _create_new_line(self, line):
        """Add new data to existing line.

        args:
            line(str): 1 line / record from the tsv.
        """
        new_data = []
        for key in sorted(self.ner_dict.keys()):
            value = self.ner_dict[key]
            new_data.append(self._join_field(value))
        new_line_segment = "\t".join(new_data)
        old_line_segment = line.replace("\n", "").replace("\r", "")
        if not old_line_segment.endswith("\t"):
            old_line_segment += "\t"
        self.lines.append(old_line_segment + new_line_segment + "\n")
        self.new_lines += 1

    def _join_field(self, value_list):
        """Join values of given list.

        args:
            value_list(list): list with string values.
        """
        return "|".join(value_list)
