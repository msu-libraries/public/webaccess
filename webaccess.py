# -*- coding: utf-8 -*-

import requests
import time


class WebAccess():

    """General class for accessing html content via the Web."""

    def __init__(self):
        pass

    @staticmethod
    def get_url(url, sleep_time, data=None):
        """Return content from specified URL.

        args:
            url(str): url to access.
            sleep_time(float): time to wait following retrieval.
        """
        response = requests.get(url, data=data)
        if response.ok:
            return response.content
        else:
            print "Request failed: {0}, {1} for {2}: "\
                  .format(response.status_code, response.reason, response.url)
            return None

    def get_html(self, url, sleep_time=0.5):
        """
        Access supplied URL and return html page content.

        args:
        url(str) -- url of any page
        sleep_time(float) -- time to sleep, in seconds, following HTTP request
        """
        page_request = requests.get(url)
        if not page_request.ok:
            print "Request for data at {0} returned error: {1}"\
                  .format(url, page_request.status_code)

        else:
            time.sleep(sleep_time)
            return page_request.text
